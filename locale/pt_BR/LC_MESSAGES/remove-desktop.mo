��          \      �       �   3   �      �        
        )     6  B   Q  �  �  A   u      �     �     �       $     g   ;                                       1) Application Menu|2) Desktop Shortcut|3) Personal Desktop Manager Item Location:CB Menu Files Menu Manager Select the items to remove Select the location you would like to remove the desktop file from Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-04-13 11:08+0000
Last-Translator: marcelo cripe <marcelocripe@gmail.com>, 2020,2022
Language-Team: Portuguese (Brazil) (http://www.transifex.com/anticapitalista/antix-development/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: pt_BR
Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 1) Menu do Aplicativo2) Atalho para a Área de Trabalho3) Pessoal Gerenciador da Área de Trabalho Localização do item:CB Arquivos de Menu Gerenciador do Menu Selecione os itens a serem removidos Selecione o local de onde você pretende remover o atalho (arquivo “.desktop”) da área de trabalho 