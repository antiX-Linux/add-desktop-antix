��          \      �       �   3   �      �        
        )     6  B   Q  �  �  E   f     �     �     �     �  #     J   ,                                       1) Application Menu|2) Desktop Shortcut|3) Personal Desktop Manager Item Location:CB Menu Files Menu Manager Select the items to remove Select the location you would like to remove the desktop file from Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2015-04-13 11:08+0000
Last-Translator: Roberto Saravia <saravia.jroberto@gmail.com>, 2015
Language-Team: Spanish (Spain) (http://www.transifex.com/anticapitalista/antix-development/language/es_ES/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es_ES
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 1) Aplicación del Menú|2) Acceso directo del escritorio|3) Personal Administrador de escritorio Localización del Elemento:CB Archivos del menú Gestor de Menu Seleccione los elementos a eliminar Seleccione la ubicación de la que desea eliminar el archivo de escritorio 