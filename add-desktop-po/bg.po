# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Peyu Yovev (Пею Йовев) <spacy00001@gmail.com>, 2019
# Tony Ivanov <duskull88@fastmail.fm>, 2014
msgid ""
msgstr ""
"Project-Id-Version: antix-development\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-04-13 13:55+0300\n"
"PO-Revision-Date: 2014-03-14 10:48+0000\n"
"Last-Translator: Peyu Yovev (Пею Йовев) <spacy00001@gmail.com>, 2019\n"
"Language-Team: Bulgarian (http://www.transifex.com/anticapitalista/antix-development/language/bg/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: bg\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: add-desktop.sh:15
msgid "Item Name:"
msgstr "Име на елемента"

#: add-desktop.sh:15
msgid "My App"
msgstr "My App"

#: add-desktop.sh:15
msgid "Item Icon:FL"
msgstr "Иконка на елемента:FL"

#: add-desktop.sh:15
msgid "Item Category:CB"
msgstr "Категория на елемента:CB"

#: add-desktop.sh:15
msgid ""
"Application|AudioVideo|Development|Education|Game|Graphics|Network|Office|Settings|System|Utility"
msgstr "Приложение|АудиоВидео|Разработка|Образование|Игра|Графика|Мрежа|Офис|Настройки|Система|Инструменти"

#: add-desktop.sh:15
msgid "Item Command:"
msgstr "Команда на елемента:"

#: add-desktop.sh:15
msgid "Item Location:CB"
msgstr "Локация на елемента:CB"

#: add-desktop.sh:15
msgid "1) Application Menu|2) Desktop Shortcut|3) Personal"
msgstr "1) Меню с приложения|2) Връзки на работен плот|3) Лично"

#: add-desktop.sh:15
msgid "File Name:"
msgstr "Име на файла:"

#: add-desktop.sh:15
msgid "Launch in Terminal:CHK"
msgstr "Стартирай в терминал:CHK"

#: add-desktop.sh:15
msgid "add-desktop"
msgstr "add-desktop"
